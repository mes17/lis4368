> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications

## Matthew Stoklosa

### LIS4368  Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/mes17/lis4368/src/master/a1/README.md)

    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - provide git command descriptions

2. [A2 README.md](https://bitbucket.org/mes17/lis4368/src/master/a2/README.md)

    - Create Index.html
    - Create Querybook.html
    - Create HelloServlet.java
    - Create AnotherHelloServlet.java
    - Create QueryServelt.java

3. [A3 README.md](https://bitbucket.org/mes17/lis4368/src/master/a3/README.md)

    - Entity Relationship Diagram (ERD)
    - Include data (at least 10 recprds each table)
    - Provide Bitbucket read-only access to repo (Language SQL) *must* include README.md. using Markdown syntax, and include links to all of the following files (From README.md):
        - docs folder: a3.mwb and a3.sql
        - img folder: a3.png (export a3.mwb file as a3.png)
        - README.md (*MUST* display a3.png ERD)
    - Canvas Links: Bitbucket repo

4. [A4 README.md](https://bitbucket.org/mes17/lis4368/src/master/a4/README.md)

    - Edit and compile CustomerServlet.java and Customer.java
    - Screenshots of Assignemet
        - Screenshot of A4 Failed Validation
        - Screenshot of A4 Passed Validation
    - Canvas Links: Bitbucket repo

5. [A5 README.md](https://bitbucket.org/mes17/lis4368/src/master/a5/README.md)

    - Edit A5 webpage
    - Show data was entreated into database that was created in A3
    - Screenshots of Assignemet
        - Screenshot of A5 Vaild User Form Entry
        - Screenshot of A5 Passed Validation
        - Screenshot of Associated database entry
    - Canvas Links: Bitbucket repo

6. [P1 README.md](https://bitbucket.org/mes17/lis4368/src/master/p1/README.md)

    - Add the following jQuery validation and regular expressions--as per the entity attribute requirements
    - Screenshots of Project
    - Canvas Links: Bitbucket repo

7. [P2 README.md](https://bitbucket.org/mes17/lis4368/src/master/p2/README.md)

    - Edit and compile CustomerServlet.java, and CustomerDB.java
    - Edit modify.jsp, customerform.jsp, customers.jsp
    - Screenshots of Project
        - Screenshot of P2 Vaild User Form Entry (customerform.jsp)
        - Screenshot of P2 Passed Validation (thanks.jsp)
        - Screenshot of P2 Display Data (customers.jsp)
        - Screenshot of P2 Modify Form (modify.jsp)
        - Screenshot of P2 Modified Data (customers.jsp)
        - Screenshot of P2 Delete Warning (customers.jsp)
        - Screenshot of P2 Associated Database Changes (Select, Insert, Update, Delete)
    - Canvas Links: Bitbucket repo

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")