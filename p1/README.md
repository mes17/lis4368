> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Matthew Stoklosa

### Project 1 Requirements:

*Deliverables:*

*Four Parts:*

1. Add the following jQuery validation and regular expressions--as per the entity attribute requirements
2. Screenshots of Project
    - Screenshot of P1 LIS4368 Portal (Main/SplashPage)
    - Screenshot of P1 Failed Validation
    - Screenshot of P1 Passed Validation
3. Canvas Links: Bitbucket repo
4. http://localhost:9999/lis4368/p1/index.jsp

#### README.md file should include the following items:

- Screenshot of P1 LIS4368 Portal (Main/SplashPage)
- Screenshot of P1 Failed Validation
- Screenshot of P1 Passed Validation

#### Project Screenshot:

*P1 LIS4368 Portal (Main/SplashPage)*:
![P1 LIS4368 Portal (Main/SplashPage)](img/p1_main.png "P1 LIS4368 Portal (Main/SplashPage)")

*P1 Failed Validation*:
![P1 Failed Validation](img/p1_failed.png "P1 Failed Validation")

*P1 Passed Validation*:
![P1 Passed Validation](img/p1_passed.png "P1 Passed Validation")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[P1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")