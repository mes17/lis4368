> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Matthew Stoklosa

### Assignment 1 Requirements:

*Three Parts:*

1. Distibuted Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (ch 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above)
* Screenshot pf running http://localhost:9999 (#2 above, Step #4(b) in tutorial):
* git commands w/short descriptions:
* Bitbucket repo links a) this assignment and b) the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates a new local repository with the specified name
2. git status - Lists all new or modified files to be commited
3. git add - Snapshots the file in preparation for versioning
4. git commit - Records file snapshots permanently in version history  
5. git push - Uploads all local branch commits to GitHub
6. git pull - Downloads bookmark history and incorporates changes
7. git branch - Lists all local branches in the current repository

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![Screenshot of running java Hello](img/a1_java.png)

*Screenshot of running Tomcat*:

![Screenshot of running Tomacat](img/a1_tomcat.png)

*Screenshot of running http://localhost:9999/lis4368/a1*:

![Screenshot of running http://localhost:9999/lis4368/a1](img/a1_local.png)

*Screenshot of AMPPS running*:

![Screenshot of AMMPS running](img/ampps_r.png)

#### Tutorial Links:

*Local Hosts:9999/lis4368*
[Localhost:9999/lis4368](http://localhost:9999/lis4368/index.jsp "Local host")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")