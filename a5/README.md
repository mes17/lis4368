> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Matthew Stoklosa

### Assignment 5 Requirements:

*Deliverables:*

*Four Parts:*

1. Edit and compile CustomerServlet.java, Customer.java, ConnectionPool.java, CustomerDB.java, and DBUtil.java
2. Screenshots of Project
    - Screenshot of A5 Vaild User Form Entry
    - Screenshot of A5 Passed Validation
    - Screenshot of Associated database entry
3. Canvas Links: Bitbucket repo
4. http://localhost:9999/lis4368/customerform.jsp?assign_num=a5

#### README.md file should include the following items:

- Screenshot of A5 Vaild User Form Entry
- Screenshot of A5 Passed Validation
- Screenshot of Associated database entry


#### Assignment Screenshot:

*A5 Vaild User Form Entry*:
![A5 Vaild User Form Entry](img/a5_vufe.png "A5 Vaild User Form Entry")

*A5 Passed Validation*:
![A5 Passed Validation](img/a5_passed.png "A5 Passed Validation")

*Associated Database Entry*
![Associated Database Entry](img/a5_ADE.png "Associated Database Entry")
