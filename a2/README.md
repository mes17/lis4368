> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Matthew Stoklosa

### Assignment 2 Requirements:

*Three Parts:*

1. Description
2. Development Environment

    1. MySQLClient Login Using AMPPS
    2. Finish the following tutorial: https://www.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html

3. Chapter Questions (ch 5,6)

### Description:

A data-driven website is one of the most common(and, demanded)typesof application designs. A website with its content stored in HTML files is referred to as a "static" website, whereas a data-driven web applicationis referred to as a "dynamic"website.The “dynamic” aspect refers to information being culled from some type of “data store”—for example, a file, database, or even data warehouse—via a web application, andgenerally, can perform “CRUD” operations: thatis, Create, Read, Update, and Delete. Moreover, any data that has been modified, is “dynamically” readin real-time.If the data stored in the data store changes, the web page(s) connected to the data storewill also change.

#### README.md file should include the following items:
#### Assessment:the following links should properly display(see screenshots below):

a. http://localhost:9999/hello (Displays directory! It should not! Needs index.html)

b. http://localhost:9999/hello/HelloHome.html (Rename"HelloHome.html" to "index.html"so that users cannot see your files!)

c. http://localhost:9999/hello/sayhello (invokes HelloServlet) Note: /sayhello maps to HelloServlet.class(changed web.xml file)

d. http://localhost:9999/hello/querybook.html

e. http://localhost:9999/hello/sayhi (invokes AnotherHelloServlet)

* screenshot of the query results http://localhost:9999/hello/querybook.html

#### Assignment Screenshots:
*Screenshot of running http://localhost:9999/hello*:

![Screenshot of running http://localhost:9999/hello](img/a2_hello.png)

*Screenshot of running http://localhost:9999/hello/HelloHome.html*:

![Screenshot of running http://localhost:9999/hello/HelloHome.html](img/a2_HelloHome.png)

*Screenshot of running http://localhost:9999/hello/sayhello*:

![Screenshot of running http://localhost:9999/hello/sayhello](img/a2_sayhello.png)

*Screenshot of running http://localhost:9999/hello/sayhi*:

![Screenshot of running http://localhost:9999/hello/sayhi](img/a2_sayhi.png)

*Screenshot of running http://localhost:9999/hello/querybook.html*:

![Screenshot of running http://localhost:9999/hello/querybook.html](img/a2_querybook.png)

*Screenshot of running http://localhost:9999/hello/querybook_selected.html*:

![Screenshot of running http://localhost:9999/hello/querybook_selected.html](img/a2_querybook_selected.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A2 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")