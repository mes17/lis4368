> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Matthew Stoklosa

### Assignment 3 Requirements:

*Deliverables:*

*Five Parts:*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 recprds each table)
3. Provide Bitbucket read-only access to repo (Language SQL) *must* include README.md. using Markdown syntax, and include links to all of the following files (From README.md):
    - docs folder: a3.mwb and a3.sql
    - img folder: a3.png (export a3.mwb file as a3.png)
    - README.md (*MUST* display a3.png ERD)
4. Canvas Links: Bitbucket repo
5. http://localhost:9999/lis4368/a3/index.jsp

#### README.md file should include the following items:

- Screenshot of A3 ERD that links to the image:

#### Assignment Screenshot and Links:
*Screenshot A3 ERD*:

![A3 ERD](img/a3_ERD.png "ERD based upon A3 Requirements")

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A3 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")