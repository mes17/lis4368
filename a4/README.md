> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Matthew Stoklosa

### Assignemet 4 Requirements:

*Deliverables:*

*Four Parts:*

1. Edit and compile CustomerServlet.java and Customer.java
2. Screenshots of Project
    - Screenshot of A4 Failed Validation
    - Screenshot of A4 Passed Validation
3. Canvas Links: Bitbucket repo
4. http://localhost:9999/lis4368/customerform.jsp?assign_num=a4

#### README.md file should include the following items:

- Screenshot of A4 Failed Validation
- Screenshot of A4 Passed Validation

#### Assignemet Screenshot:

*A4 Failed Validation*:
![A4 Failed Validation](img/a4_failed.png "A4 Failed Validation")

*A4 Passed Validation*:
![A4 Passed Validation](img/a4_passed.png "A4 Passed Validation")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A4 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mes17/bitbucketstationlocation/src/ "Bitbucket Station Locations")