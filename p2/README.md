> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Matthew Stoklosa

### Project 2 Requirements:

*Deliverables:*

*Five Parts:*

1. Edit and compile CustomerServlet.java, and CustomerDB.java
2. Edit modify.jsp, customerform.jsp, customers.jsp
3. Screenshots of Project
    - Screenshot of P2 Vaild User Form Entry (customerform.jsp)
    - Screenshot of P2 Passed Validation (thanks.jsp)
    - Screenshot of P2 Display Data (customers.jsp)
    - Screenshot of P2 Modify Form (modify.jsp)
    - Screenshot of P2 Modified Data (customers.jsp)
    - Screenshot of P2 Delete Warning (customers.jsp)
    - Screenshot of P2 Associated Database Changes (Select, Insert, Update, Delete)
4. Canvas Links: Bitbucket repo
5. http://localhost:9999/lis4368/customerform.jsp?assign_num=p2

#### README.md file should include the following items:

- Screenshot of P2 Vaild User Form Entry (customerform.jsp)
- Screenshot of P2 Passed Validation (thanks.jsp)
- Screenshot of P2 Display Data (customers.jsp)
- Screenshot of P2 Modify Form (modify.jsp)
- Screenshot of P2 Modified Data (customers.jsp)
- Screenshot of P2 Delete Warning (customers.jsp)
- Screenshot of P2 Associated Database Changes (Select, Insert, Update, Delete)

#### Assignment Screenshot:

|          *P2 Vaild User Form Entry (customerform.jsp)*:               |                *P2 Passed Validation (thanks.jsp)*:            |
:----------------------------------------------------------------------:|:---------------------------------------------------------------:
![P2 Vaild User Form Entry](img/p2_vufe.png "P2 Vaild User Form Entry") | ![P2 Passed Validation](img/p2_pass.png "P2 Passed Validation")

|           *P2 Display Data (customers.jsp)*:           |            *P2 Modify Form (modify.jsp)*:              |
:-------------------------------------------------------:|:-------------------------------------------------------:
![P2 Display Data](img/p2_display.png "P2 Display Data") | ![P2 Modify Form](img/p2_modified.png "P2 Modify Form")

|          *P2 Modified Data (customers.jsp)*:            |                *P2 Delete Warning (customers.jsp)*:        |
:--------------------------------------------------------:|:-----------------------------------------------------------:
![P2 Modified Data](img/p2_update.png "P2 Modified Data") | ![P2 Delete Warning](img/p2_delete.png "P2 Delete Warning")

|         *P2 Associated Database Changes (Select, Insert, Update, Delete)*:             |
:---------------------------------------------------------------------------------------:|
![P2 Associated Database Changes](img/p2_database.png "P2 Associated Database Changes")  |